package com.journaldev.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.journaldev.spring.model.Person;
import com.journaldev.spring.service.PersonService;

@Controller
public class PersonController {
	
	private PersonService personService;
	
	@Autowired(required=true)
	@Qualifier(value="personService")
	public void setPersonService(PersonService ps){
		this.personService = ps;
	}
	
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public @ResponseBody List<Person> listPersons() {
		List<Person> personList = new ArrayList<Person>();
		personList = personService.listPersons();
		
		return personList;
	}
	
	//For add and update person both
	@RequestMapping(value= "/person/addORupdate", method = RequestMethod.POST)
	public @ResponseBody List<Person> addPerson(@RequestBody Person p){
		
		if(p.getId() == 0){
			//new person, add it
			
		this.personService.addPerson(p);
		}else{
			//existing person, call update
			this.personService.updatePerson(p);
		}
		List<Person> personList = new ArrayList<Person>();
		personList = this.personService.listPersons();
		
		return personList;
		
		
	}
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
    @RequestMapping("/edit/{id}")
    public @ResponseBody Person editPerson(@PathVariable("id") int id){
        
        Person person=new Person();
        person = this.personService.getPersonById(id);
        return person;
    }
	
}
